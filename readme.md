- Setup

please make sure using SLC6 machines (lxplus6), not fully developed for lxplus7 due to the CentOS7 status.

```
setupATLAS -c centos7
asetup AthGeneration,21.6.55
```

- run local job
	- Change the run directory name and the configurations as you like
```
mkdir run; cd run
Gen_tf.py --ecmEnergy=13000. --maxEvents=10000 --firstEvent=1 --randomSeed=123456 --outputEVNTFile=output.root --jobConfig=../100002/
```

- run on the grid
```
tar -czvf mg5gen.tgz 100002/
pathena --trf "Gen_tf.py --ecmEnergy=13000. --maxEvents=-1 --firstEvent=1 --randomSeed=%RNDM:1000 --jobConfig=100002/ --outputEVNTFile=%OUT.root" --outDS=user.${USER}.`date +'%Y%m%d'`.singleTop_fourAngle.mg5pythia8.evtGen.raw.root --nEventsPerJob=100 --split=1000 --inTarBall=mg5gen.tgz
```

- Other info:

JIRA ticket: TBD